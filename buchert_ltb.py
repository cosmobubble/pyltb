#!/usr/bin/python
"""Find Buchert averages of quantities in some LTB model"""
import numpy as np
import scipy.integrate as integ
from units import *

BUCHERT_TOL = 1e-14

class Buchert(object):
	"""A class which takes the Buchert volume average of some LTB model over 
	   some domain rD at some time t."""
	def __init__(self, model, umax_multiplier=4.):
		self.M = model
		self.V0 = 1.
		# Affine distance to integrate out to when solving the Sach equations 
		# (affects speed and max. redshift)
		self.UMAX_MULTIPLIER = umax_multiplier
		
		# Define constants used in Sachs ODE evolution code
		self.UMAX0 = -400. # Starting value for optimal ending affine parameter
		self.UCELLS = 200 # No. affine param. cells to calc. (affects region matching acc.)
	
	def update_volume_normalisation(self, rD, t0):
		"""Find the averaged volume at t=t0 for a given domain size."""
		self.V0 = self.volume(rD, t0)
		
	
	def q(self, rD, t):
		"""Get Buchert q_D, acceleration parameter of averaged spatial 
		   hypersurfaces over domain of radius rD, at time t"""
		
		# Build empty lists, where the first element is zero (starting point 
		# for integration)
		vol = [0.0]
		expn = [0.0]
		expn2 = [0.0]
		rho = [0.0]
		shr = [0.0]
		ricci3 = [0.0]
		qd = [0.0]
		
		# For an ordered list of domain radii, compute successive integrals step-by-step
		for j in range(len(rD)-1):
			r1 = rD[j]
			r2 = rD[j+1]
		
			# Get domain volume by integrating volume element
			v = integ.romberg( self.integ_dvol, r1, r2, args=(t,) )
			
			# Calculate averages: expansion, expansion^2, shear, density
			# For each integral, add to the previous value
			e1 = integ.romberg( self.integ_expansion,  r1, r2, args=(t,) )
			e2 = integ.romberg( self.integ_expansion2, r1, r2, args=(t,) )
			rh = integ.romberg( self.integ_density,    r1, r2, args=(t,) )
			sh = integ.romberg( self.integ_shear,      r1, r2, args=(t,) )
			ri = integ.romberg( self.integ_ricci3,     r1, r2, args=(t,) )
		
			# Add current contributions to previous ones
			vol.append( vol[-1] + v )
			expn.append( expn[-1] + e1/v )
			expn2.append( expn2[-1] + e2/v )
			rho.append( rho[-1] + rh/v )
			shr.append( shr[-1] + sh/v )
			ricci3.append( ricci3[-1] + ri/v )
			
			# Calculate big Q, Q = (2/3)(<th^2> - <th>^2) - 2<sig^2>
			Q = (2./3.)*( expn2[-1] - (expn[-1]**2. / v) ) - 2.*shr[-1]
			
			# Calculate Buchert qD
			_qd = -(-4.*np.pi*G*rho[-1] + Q)
			_qd /= (8.*np.pi*G*rho[-1] - 0.5*ricci3[-1]*C**2. - 0.5*Q)
			qd.append(_qd)
			
		return qd
	
	
	def q_single(self, rD, t):
		"""Get Buchert q_D, acceleration parameter of averaged spatial 
		   hypersurfaces over domain of radius rD, at time t"""
		r1 = 0.0
		r2 = rD
		
		# Get domain volume by integrating volume element
		v = integ.romberg( self.integ_dvol, r1, r2, args=(t,), tol=BUCHERT_TOL/10 )
		
		# Calculate averages: expansion, expansion^2, shear, density
		expn = integ.romberg( self.integ_expansion,  r1, r2, args=(t,), tol=BUCHERT_TOL )
		expn2 = integ.romberg( self.integ_expansion2, r1, r2, args=(t,), tol=BUCHERT_TOL )
		rho = integ.romberg( self.integ_density,    r1, r2, args=(t,), tol=BUCHERT_TOL )
		shr = integ.romberg( self.integ_shear,      r1, r2, args=(t,), tol=BUCHERT_TOL )
		ricci3 = integ.romberg( self.integ_ricci3,     r1, r2, args=(t,), tol=BUCHERT_TOL )
		
		# Calculate big Q, Q = (2/3)(<th^2> - <th>^2) - 2<sig^2>
		Q = (2./3.)*( expn2 - (expn**2. / v) ) - 2.*shr
			
		# Calculate Buchert qD
		_qd = (4.*np.pi*G*rho - Q)
		_qd /= (8.*np.pi*G*rho - 0.5*ricci3*C**2. - 0.5*Q)
		
		return _qd, expn, expn2, rho, shr, ricci3, Q, v

	def q_ks(self, rD, t):
		"""Get Clarkson and Umeh q_KS == 'Q', averaged Kristian-Sachs 
		   deceleration parameter monopole, over domain of radius rD, at 
		   time t"""
		r1 = 0.0
		r2 = rD
		
		# Get domain volume by integrating volume element
		v = integ.romberg( self.integ_dvol, r1, r2, args=(t,), tol=BUCHERT_TOL/10 )
		qks = integ.romberg( self.integ_qks,  r1, r2, args=(t,), tol=BUCHERT_TOL )
		
		return qks/v
	
	def q_local(self, rD, t):
		"""Get Clarkson and Umeh q_local, averaged local volume deceleration 
		   parameter monopole, over domain of radius rD, at time t"""
		r1 = 0.0
		r2 = rD
		
		# Get domain volume by integrating volume element
		v = integ.romberg( self.integ_dvol, r1, r2, args=(t,), tol=BUCHERT_TOL/10 )
		qloc = integ.romberg( self.integ_qlocal,  r1, r2, args=(t,), tol=BUCHERT_TOL )
		
		return qloc/v
	
	def volume(self, rD, t):
		"""Get the integrated volume at a given time."""
		V = integ.romberg( self.integ_dvol, 0.0, rD, args=(t,), tol=BUCHERT_TOL )
		return V
	
	def aD(self, rD, t):
		"""Unnormalised Buchert scale factor at some time, for some domain size."""
		return (self.volume(rD, t)/self.V0)**(1./3.)
	
	def ricci(self, rD, t):
		"""Averaged 3-Ricci curvature at a given time, for a given averaging domain."""
		vol = self.volume(rD, t)
		ricci = integ.romberg( self.integ_ricci3, 0.0, rD, args=(t,), tol=BUCHERT_TOL )
		return ricci / vol
	
	def quick_aD_ricci(self, rD, t):
		"""Calculate aD and <^3R> at the same time, to save time."""
		vol = self.volume(rD, t)
		aD = (vol/self.V0)**(1./3.)
		ricci = integ.romberg( self.integ_ricci3, 0.0, rD, args=(t,), tol=BUCHERT_TOL )
		ricci /= vol
		return aD, ricci, vol
	
	
	
	def quick_sachs_variables(self, rD, t):
		"""Calculate aD and <^3R> at the same time, to save time."""
		dt = 10. # Myr
		
		# Volume
		vol = self.volume(rD, t)
		vol2 = self.volume(rD, t+dt)
		
		# Scale factor
		aD = (vol/self.V0)**(1./3.)
		aD2 = (vol2/self.V0)**(1./3.)
		
		# Hubble rate
		dadt = (aD2 - aD) / dt
		H = dadt / aD
		
		# Density
		rho = integ.romberg( self.integ_density, 0.0, rD, args=(t,), tol=BUCHERT_TOL )
		rho /= vol
		
		return aD, H, rho, vol
	
	
	
	def get_volume(self, rD):
		"""TEST: Get integrated volume for offcentre integration region."""
		t = 1e4
		r1 = 0.0
		r2 = rD
		V = integ.romberg( self.integ_dvol, r1, r2, args=(t,), tol=BUCHERT_TOL )
		print "V =", V
		return V
	
	
	############################################################################
	# Integrands of functions multiplied by the volume element
	############################################################################
	
	def integ_dvol(self, r, t):
		"""Radial part of the spatial volume element (for central observer)
		   Sqrt(det h_ab)d^3x, where h_ab is spatial projection tensor"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		return a1**2. * a2 * r**2. / np.sqrt( 1. - self.M.k(r) * r**2. )
	
	def integ_density(self, r, t):
		"""Physical density rho"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - self.M.k(r) * r**2. )
		return vol * (3.*self.M.m(r) + self.M.mprime(r)*r) / ( 3.*a2*(a1**2.) )
	
	def integ_expansion(self, r, t):
		"""Expansion scalar for a dust congruence"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		a1dot = self.M.a1dot(r,t)
		a2dot = self.M.a2dot(r,t)
		k = self.M.k(r)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
		return vol * ((a2dot/a2) + 2.*(a1dot/a1))
	
	def integ_expansion2(self, r, t):
		"""Squared expansion scalar for a dust congruence"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		a1dot = self.M.a1dot(r,t)
		a2dot = self.M.a2dot(r,t)
		k = self.M.k(r)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
		return vol * ((a2dot/a2) + 2.*(a1dot/a1))**2.
	
	def integ_shear(self, r, t):
		"""Shear scalar for a dust congruence, sigma^2 = (1/2)sigma^ab sigma_ab
		   (Note the factor of 2 in the definition)"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		a1dot = self.M.a1dot(r,t)
		a2dot = self.M.a2dot(r,t)
		k = self.M.k(r)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
		return vol * ( (a2dot/a2) - (a1dot/a1) )**2.
	
	def integ_ricci3(self, r, t):
		"""Spatial Ricci 3-scalar"""
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		k = self.M.k(r)
		kprime = self.M.kprime(r)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
		## OLD: return vol * (2./a1)*( k + (2.*k/a2) + (kprime*r/a2) )
		return vol * (2./a1)*( (k/a1) + (2.*k/a2) + (kprime*r/a2) )
	
	############################################################################
	# Integrands of other functions to be averaged
	############################################################################
	
	def integ_qks(self, r, t):
		"""The Kristian-Sachs observational deceleration parameter, Q, defined 
		   in Clarkson and Umeh 2011."""
		ht = self.M.Ht(r,t)
		hr = self.M.Hr(r,t)
		qq = 3. / (2.*ht + hr)**2.
		qq *= 4.*np.pi*G*self.M.density(r, t) + 2.*( hr - ht )**2.
		
		# Volume
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		k = self.M.k(r)
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
		return vol * qq
	
	def integ_qlocal(self, r, t):
		"""The local volume deceleration parameter, q_theta, defined in 
		   Clarkson and Umeh 2011."""
		rho = self.M.density(r,t)
		ht = self.M.Ht(r,t)
		hr = self.M.Hr(r,t)
		k = self.M.k(r)
		kprime = self.M.kprime(r)
		a1 = self.M.a1(r,t)
		a2 = self.M.a2(r,t)
		
		vol = a1**2. * a2 * r**2. / np.sqrt( 1. - k * r**2. )
		ricci3 = ( 3./(a1*a2) ) * ( k*(2. + (a2/a1)) + kprime*r ) # Modified by factor 3/2
		shr = (hr - ht)**2. # Modified by factor of 3
		
		qloc = (12.*np.pi*G*rho + 2.*shr) / (24.*np.pi*G*rho + shr - ricci3*C**2.)
		return vol * qloc
	
	#def average(self, fn):
	#	"""Take Buchert average on some time slice for a spherical domain of 
	#	   radius rD"""
	#	integ.romberg( fnIntegrand, 0.0, self.rD, args=(fn, self.t) ) / VD


	################################################################################
	# Solve geodesic equation in effective averaged geometry
	################################################################################
	
	def eff_geod_eqn_kt(self, x, t, rD):
		"""Integrand for ODE solver for the effective geometry geodesic eqn., 
		   with k=<k>(t)."""
		aa, r3, vol = self.quick_aD_ricci(rD, t)
		kk = r3 * aa**2. / (6. * C**2.) # From averaged 3-Ricci scalar
		dxdt = C * np.sqrt(1. - kk*x**2.) / aa
		return dxdt
	
	def eff_geod_eqn_k0(self, x, t, rD):
		"""Integrand for ODE solver for the effective geometry geodesic eqn., 
		   with assumed effective spatial flatness k=0."""
		aa = self.aD(rD, t)
		dxdt = C / aa
		return dxdt
	
	def eff_geod_eqn_kconst(self, x, t, k0, rD):
		"""Integrand for ODE solver for the effective geometry geodesic eqn., 
		   with k=const=<k>(t0)."""
		aa = self.aD(rD, t)
		dxdt = C * np.sqrt(1. - k0*x**2.) / aa
		return dxdt
	
	def effective_da(self, rD, t):
		"""Get comoving coordinate x(t) in the effective geometry defined by 
		   the 1D Buchert-like average line element 
		   ds^2 = dt^2 - a^2_D dx^2/(1 - <k>x^2)"""
		
		# Initial conditions
		t0 = t[0] #self.mod1.t(1.0)
		k0 = self.ricci(rD, t0) / (6. * C**2.)
		x0 = [0.0]
		
		# Get x(t) for different choices of spatial curvature k
		x1 = integ.odeint(self.eff_geod_eqn_kt, x0, t, args=(rD,))
		x2 = integ.odeint(self.eff_geod_eqn_k0, x0, t, args=(rD,))
		x3 = integ.odeint(self.eff_geod_eqn_kconst, x0, t, args=(k0, rD))
		
		# Get average s-t scale factor for each t, and redshift from averaged scale factor
		aa = [self.aD(rD, tt) for tt in t]
		aa = np.array(aa)
		zz = (1./aa) - 1.
		
		# Construct angular diameter distance
		da1 = -x1.flatten() * aa
		da2 = -x2.flatten() * aa
		da3 = -x3.flatten() * aa
		
		return da1, da2, da3, zz, aa
	
	
	############################################################################
	# TESTING: Use Sachs equations instead of coordinate r(t) * a to find dA|avg
	############################################################################
	
	def eqn_geodesic_avg(self, y, u, rD):
		"""RHS of linear ODEs for Sachs equations and other geodesic quantities in 
		   a given FLRW model. Quantities are:
			(0) S = sqrt(A)
			(1) X = dS/du
			(2) z(u) [redshift along LOS, not necessarily a = 1/(1+z)]
			(3) x(u)
			(4) t(u)
		"""
		# Label coordinates
		S = y[0]; X = y[1]; z = y[2]; x = y[3]; t = y[4]
		fn = [None]*len(y) # Empty array for values being returned
	
		# Calculate background quantities
		a, H, rho, vol = self.quick_sachs_variables(rD, t)
		
		# Calculate RHS of ODEs
		# FIXME: Eqn. for r(lambda) needs to be fixed.
		fn[0] = X
		fn[1] = (-4.*np.pi*G/(C**2.)) * rho * (1.+z)**2. * S
		fn[2] = -(H/C) * (1. + z)**2.
		fn[3] = 0.0 # -((1.+z) / a) * ( 1. + 0.25 * mod.Ok * (H0*x/C)**2. )
		fn[4] = (1. + z)/C
		return np.array(fn)
	
	def effective_da_sachs(self, rD, t0):
		"""Integrate the Sachs equations for the averaged spacetime"""
		y0 = np.array([0.0, 1.0, 0.0, 0.0, t0])
		u = np.linspace(0.0, self.UMAX0*self.UMAX_MULTIPLIER, self.UCELLS)
		y = integ.odeint(self.eqn_geodesic_avg, y0, u, args=(rD,))
		y = y.T # Get transpose of results matrix
		return y
		
