#!/usr/bin/python
"""Spherical Collapse model constructed from disjoint FLRW models."""

import numpy as np
import scipy.integrate
import scipy.interpolate
from units import *

# NOTE: Code validated on 2012-01-17. Comparison with analytic forms of the 
# angular diameter distance for Milne and EdS FLRW models produced good 
# agreement. Agreement with numerical result using dA = a*r(z) for both 
# FLRW models was also good (except for a problem with Milne result, which 
# seems to be due to a problem with the FLRW numerical code, not this code).
# Difference between dA calculated with this code, and with analytic result, 
# is ~ 1 Mpc at z~ 1 (0.1%).
# The boundary matching between FLRW regions also seems to be good. I compared 
# dA(z) calculated with a Milne model over one large domain, with dA(z) 
# calculated with 350 smaller domains each with the same Milne model. The 
# agreement was excellent (better than the agreement between this code and the 
# analytic result).

class SphericalCollapse(object):
	"""Spherical collapse model with ray-tracing.
	
	 - *chi1,2* is the comoving size of each cell
	 
	 - *ncells* is the number of cells
	 
	 - *mod1* and *mod2* are the FLRW models that should be used in alternating cells.
	 """
	
	def __init__( self, chi1, chi2, ncells, mod1, mod2, 
					    initialfrac=1., debug=False, t0mode="mod1", nn=1.,
					    umax_multiplier=6.5 ):
		"""Spherical collapse model with ray-tracing."""
		
		# Define cell properties
		self.chi1 = chi1; self.chi2 = chi2
		self.chi0 = chi1 + chi2 # Total comoving domain size
		self.NCELLS = ncells # Number of cells
		
		# Affine distance to integrate out to when solving the Sach equations 
		# (affects speed and max. redshift)
		self.UMAX_MULT = umax_multiplier
		
		# TESTING
		self.nn = nn # Exponent of volume weighting
		
		# Initial cell is a special case. Define how far into the cell the 
		# starting point should be.
		self.initialfrac = initialfrac
		
		# Define models to use for each cell
		self.mod1 = mod1
		self.mod2 = mod2
		
		# Define constants used in Sachs ODE evolution code
		self.UMAX0 = -400. # Starting value for optimal ending affine parameter
		self.UCELLS = 200 # No. affine param. cells to calc. (affects region matching acc.)
		self.MAX_TRIES = 50 # Max. no. refinements to make before giving up on ODE solving
		self.INCR_FACTOR = 1.4 # Factors to inc./dec. umax by to find better affine range.
		self.DECR_FACTOR = 0.8
		
		self.debug = debug # Debugging flag. Print debugging information if true.
		
		# Set the time today, t0
		self.t0mode = t0mode
		if t0mode == "mod1":
			self.t0 = self.mod1.t(1.0) # Use t(a=1) in model 1
		elif t0mode == "mod2":
			self.t0 = self.mod2.t(1.0) # Use t(a=1) in model 2
		else:
			# Interpret 't0mode' as a float, and use that as the time today
			print "Unknown t0 mode. Interpreting as a float."
			self.t0 = float(t0mode)
		
		# Set initial values
		self.__initial_values()
	
	
	def __initial_values(self):
		"""Define initial values for the volume and so on."""
		
		# Get fraction of comoving domain size that each cell takes up
		self.f1 = self.chi1 / self.chi0
		self.f2 = self.chi2 / self.chi0
		
		# Total coordinate region size
		self.x1 = self.mod1.coord_x(self.chi1)
		self.x2 = self.mod2.coord_x(self.chi2)
		
		# Comoving volume of each region
		self.comoving_V1 = self.mod1.comoving_volume(self.x1)
		self.comoving_V2 = self.mod2.comoving_volume(self.x2)
		
		"""
		print "\tComoving line fraction in region 1:\t", round(self.chi1 / (self.chi0), 4)
		print "\tCoordinate line fraction in region 1:\t", round(self.x1 / (self.x1 + self.x2), 4)
		print "\tVolume fraction in region 1:\t\t", round(self.comoving_V1 / (self.comoving_V1 + self.comoving_V2), 4)
		print "\n\tCoord: x1 =", self.x1, " and x2 =", self.x2
		print "\tComov: chi1 =", self.chi1, " and chi2 =", self.chi2
		print "\tProp. Vol: V1 =", self.vol1(self.t0), " and V2 =", self.vol2(self.t0)
		print "\tComov. Vol: V1 =", self.comoving_V1, " and V2 =", self.comoving_V2
		"""
		
		# Initial volume (used for normalisation)
		# Need to set self.comoving_V1,2 first.
		self.V0 = self.vol1(self.t0) + self.vol2(self.t0)
	
	def set_phase(self, phase):
		"""Set the fraction through the first region at which to start integrating."""
		self.initialfrac = phase
		

	################################################################################
	# Solve geodesic equations and output a suitable number of samples for interp.
	################################################################################

	def eqn_geodesic(self, y, u, mod):
		"""RHS of linear ODEs for Sachs equations and other geodesic quantities in 
		   a given FLRW model. Quantities are:
			(0) S = sqrt(A)
			(1) X = dS/du
			(2) z(u) [redshift along LOS, not necessarily a = 1/(1+z)]
			(3) x(u)
			(4) t(u)
		"""
		# Label coordinates
		S = y[0]; X = y[1]; z = y[2]; x = y[3]; t = y[4]
		fn = [None]*len(y) # Empty array for values being returned
	
		# Calculate background quantities
		a = mod.a(t)
		zmod = (1./a) - 1. # Redshift in model, zmod = 1/a - 1 [not necessarily z(u)]
		H = mod.H(zmod)
		H0 = mod.H0
		rho = mod.density(zmod)
		
		# Calculate RHS of ODEs
		fn[0] = X
		fn[1] = (-4.*np.pi*G/(C**2.)) * rho * (1.+z)**2. * S
		fn[2] = -(H/C) * (1. + z)**2.
		fn[3] = -((1.+z) / a) * ( 1. - 0.25 * mod.Ok * (H0*x/C)**2. ) # FIXME
		fn[4] = (1. + z)/C
	
		return np.array(fn)


	def get_evolution(self, y0, u, model):
		"""Solve the Sachs equations for array of affine parameters u, starting 
		from initial conditions 'y0', for an FLRW model 'model'."""
	
		# Integrate from the initial conditions
		y, out = scipy.integrate.odeint(self.eqn_geodesic, y0, u, args=(model,), full_output=1, mxstep=1000, mxhnil=1) # Last two args are there to suppress messages
		y = y.T # Get transpose of results matrix
		#print "\n\n", out
		return y
	
	
	def refine_evolution(self, y0, model, rbound):
		"""Try to evolve the ODEs with a good number of samples in the desired 
		   range of r values."""
		ntries = 0
		umax = self.UMAX0
		goodsample = False
	
		# Try a few times to get a good number of samples in the right range
		while not goodsample and ntries < self.MAX_TRIES:
			ntries += 1
			u = np.linspace(0.0, umax, self.UCELLS)
		
			# Evolve ODEs and then check to see if affine parameter sample was 
			# acceptable
			y = self.get_evolution(y0, u, model)
			check = self.check_boundaries(y, rbound, umax)
		
			# If result was good enough, get boundary values
			if check['good']:
				goodsample = True
				imax = check['imax']
				y0, idx = self.get_boundary_values(y, rbound, imax)
				return y, y0, idx
			else:
				umax = check['umax']
				if self.debug:
					print "\nTry", ntries, "-- umax =", umax
	
		# Quit with an error if no sensible result was found
		print "ERROR: Didn't solve the ODEs well enough. Exiting."
		exit()

	
	################################################################################
	# Handle boundaries between FLRW cells
	################################################################################

	def check_boundaries(self, y, rbound, umax):
		"""Check the values that were obtained for a given set of input boundaries 
		   for the ODE integration. Warn if they're not good enough, and suggest 
		   new boundaries."""
	
		# Find the index of the last non-NaN value. The interpolators really 
		# struggle when there are NaNs in the array.
		foundnan = False; inan = -1
		while inan < len(y[0])-1 and not foundnan:
			inan += 1
			for j in range(len(y)):
				if np.isnan(y[j][inan]):
					foundnan = True
	
		# Check if the sign of r changes. Get the index of the maximum of r.
		ibigr = (np.abs(y[3] - max(y[3]))).argmin()
	
		# Get the index closest to rbound, the boundary value of r.
		ibound = (np.abs(y[3] - rbound)).argmin()
	
		# Get the maximum index of the array
		iend = len(y[0]) - 1
	
		# Analyse the indices and decide whether they are good enough. Suggest 
		# improvements.
		good = True; suggested_umax = umax
	
		# Check if there are enough samples between 0 and rbound
		if float(ibound)/float(iend) < 0.3:
			good = False
			suggested_umax = umax * self.DECR_FACTOR # Reduce umax a bit
	
		# Check if umax is big enough to get r > rbound
		if max(y[3]) < rbound:
			good = False
			suggested_umax = umax * self.INCR_FACTOR # Increase umax a bit
	
		if self.debug:
			print "inan, ibigr, ibound, iend"
			print inan, ibigr, ibound, iend
	
		# Return appropriate values
		return {'good': good, 'umax':suggested_umax, 'imax':ibigr}
	

	def get_boundary_values(self, y, rmax, imax):
		"""For the parameters in array y, get their values at some fixed r by 
		   interpolating."""
	
		fn = [None]*len(y) # Empty array for values being returned
	
		# Maximum array index to use for interpolation is calculated by the 
		# check_boundaries() function.
		imax += 1
		S = y[0][:imax]; X = y[1][:imax]; z = y[2][:imax]
		r = y[3][:imax]; t = y[4][:imax]
	
		# Construct interpolators
		interp_S = scipy.interpolate.interp1d(r, S, kind='linear')
		interp_X = scipy.interpolate.interp1d(r, X, kind='linear')
		interp_z = scipy.interpolate.interp1d(r, z, kind='linear')
		interp_t = scipy.interpolate.interp1d(r, t, kind='linear')
		
		try:
			# Return interpolated values
			fn[0] = interp_S(rmax)		
			fn[1] = interp_X(rmax)
			fn[2] = interp_z(rmax)
			fn[3] = 0.0 #rmax
			fn[4] = interp_t(rmax)
		except:
			print "\n\n\nxxx:", S, r, rmax, imax, len(y[0])
			raise
	
		# Get index nearest to rmax
		idx = (np.abs(r - rmax)).argmin()
	
		return fn, idx


	################################################################################
	# Do the raytracing and process the results
	################################################################################
	
	def raytracing_domain_sizes(self, t):
		"""Calculate the coordinate length x_f of the next domain that the ray 
		   will be traced through. This depends on the volume fraction taken up 
		   by the region being entered at time t, and is a fraction of the 
		   comoving distance (rather than coordinate distance). The sizes for 
		   domains 1 and 2 are returned; one of these should be selected 
		   depending on which FLRW region will be raytraced through next."""
		
		# Comoving size of collapsing regions is kept fixed
		if self.t0mode=="mod1":
			x2 = self.mod2.coord_x(self.chi2)
			# Comoving size of expanding regions is modified to trace the volume fraction
			x1 = x2 * (self.mod2.a(t) / self.mod1.a(t)) * (self.vol1(t) / self.vol2(t))
		else:
			x1 = self.mod1.coord_x(self.chi1)
			# Comoving size of expanding regions is modified to trace the volume fraction
			x2 = x1 * (self.mod1.a(t) / self.mod2.a(t)) * (self.vol2(t) / self.vol1(t))
		
		return [x1, x2]


	def raytrace(self):
		"""Ray-trace through the spherical collapse model that has been defined."""
		
		# Initial parameter values, y = (S, X, z, r, t)
		# X0 = sqrt(solid angle). Need to divide S by X0 to get ang. diam. dist.
		t0 = self.t0
		y0 = np.array([0.0, 1.0, 0.0, 0.0, t0])
		r1, r2 = self.raytracing_domain_sizes(t0)
		
		# Arrays for storing results of integration
		data = []; idxs = []; boundaries = []
		boundaries.append(y0)
		
		# The first cell is a special case.
		# We may choose what fraction through the cell the starting point is.
		# FIXME: Not quite right. Should be initialfrac*chi1
		y, y0, idx = self.refine_evolution(y0, self.mod1, rbound=r1*self.initialfrac)
		data.append(y); boundaries.append(y0); idxs.append(idx)
		
		# Integrate through all the other cells
		for i in range(self.NCELLS):
			if self.debug:
				print "*"*50
				print "\t\t CELL", i
				print "*"*50
			
			# Evolve comoving domain size according to volume fraction.
			tnow = y0[4]
			r1, r2 = self.raytracing_domain_sizes(tnow)
			
			# Choose which region to raytrace through now
			if i % 2 == 0:
				y, y0, idx = self.refine_evolution(y0, self.mod2, rbound=r2)
			else:
				y, y0, idx = self.refine_evolution(y0, self.mod1, rbound=r1)
			data.append(y); boundaries.append(y0); idxs.append(idx)
		
		# Finished integration. Now, process the data so that one set of 
		# variables along the null ray is output
		data = np.array(data)
		S = np.array([]); X = np.array([]); z = np.array([])
		r = np.array([]); t = np.array([]);
		for i in range(len(data)):
			S = np.concatenate( (S, data[i][0][:idxs[i]]) )
			X = np.concatenate( (X, data[i][1][:idxs[i]]) )
			z = np.concatenate( (z, data[i][2][:idxs[i]]) )
			r = np.concatenate( (r, data[i][3][:idxs[i]]) )
			t = np.concatenate( (t, data[i][4][:idxs[i]]) )
		
		return S, X, z, r, t
	
	
	
	
	
	###################################
	## TESTING
	
	def raytrace_test(self):
		"""Ray-trace through the spherical collapse model that has been defined."""
		
		# Initial parameter values, y = (S, X, z, r, t)
		# X0 = sqrt(solid angle). Need to divide S by X0 to get ang. diam. dist.
		t0 = self.t0
		y0 = np.array([0.0, 1.0, 0.0, 0.0, t0])
		r1, r2 = self.raytracing_domain_sizes(t0)
		
		# Arrays for storing results of integration
		data = []; idxs = []; boundaries = []
		boundaries.append(y0)
		
		# The first cell is a special case.
		# We may choose what fraction through the cell the starting point is.
		# FIXME: Not quite right. Should be initialfrac*chi1
		y, y0, idx = self.refine_evolution(y0, self.mod1, rbound=r1*self.initialfrac)
		data.append(y); boundaries.append(y0); idxs.append(idx)
		
		# Integrate through all the other cells
		for i in range(self.NCELLS):
			if self.debug:
				print "*"*50
				print "\t\t CELL", i
				print "*"*50
			
			# Evolve comoving domain size according to volume fraction.
			tnow = y0[4]
			r1, r2 = self.raytracing_domain_sizes(tnow)
			
			# Choose which region to raytrace through now
			if i % 2 == 0:
				y, y0, idx = self.refine_evolution(y0, self.mod2, rbound=r2)
			else:
				y, y0, idx = self.refine_evolution(y0, self.mod1, rbound=r1)
			data.append(y); boundaries.append(y0); idxs.append(idx)
		
		# Finished integration. Now, process the data so that one set of 
		# variables along the null ray is output
		data = np.array(data)
		S = np.array([]); X = np.array([]); z = np.array([])
		r = np.array([]); t = np.array([]);
		for i in range(len(data)):
			S = np.concatenate( (S, data[i][0][:idxs[i]]) )
			X = np.concatenate( (X, data[i][1][:idxs[i]]) )
			z = np.concatenate( (z, data[i][2][:idxs[i]]) )
			r = np.concatenate( (r, data[i][3][:idxs[i]]) )
			t = np.concatenate( (t, data[i][4][:idxs[i]]) )
		
		return S, X, z, r, t, boundaries
	
	
	
	###################################
	
	
	
	
	
	################################################################################
	# Ways of weighting the line of sight length / volume
	################################################################################
	
	def h1(self, t):
		# Fraction of the total proper spatial volume taken up by region 1.
		V1 = self.vol1(t) #**(self.nn)
		V2 = self.vol2(t) #**(self.nn)
		return V1 / (V1 + V2)
		#return self.chi1**3. / (self.chi1**3. + self.chi2**3.)
	
	"""
	# FIXME: Testing.
	def h1(self, t):
		# Old version (what we used to do, sort of)
		fcell = 1. - self.comoving_V1 / (self.comoving_V1 + self.comoving_V2) #0.01
		V1 = self.mod1.a(t)**3.
		V2 = fcell * self.mod2.a(t)**3.
		return V1 / (V1 + V2)
	"""
	
	def vol1(self, t):
		"""Proper spatial volume of region 1."""
		a = self.mod1.a(t)
		return a**3. * self.comoving_V1
	
	def vol2(self, t):
		"""Proper spatial volume of region 2."""
		a = self.mod2.a(t)
		return a**3. * self.comoving_V2
	
	
	################################################################################
	# Different ways of defining the average (effective) scale factor, a_D
	################################################################################
	
	# Scale factor based on the volume average, normalised to the initial 
	# "evolving" domain size (but not with evolving domains)
	def a_avg(self, t):
		V1 = self.mod1.a(t)**3. * self.comoving_V1
		V2 = self.mod2.a(t)**3. * self.comoving_V2
		return ((V1 + V2)/self.V0)**(1./3.)
	
	"""
	# Scale factor based on the line average, with evolving comoving domain size
	def a_avg(self, t):
		r1, r2 = self.rsizes(t)
		L1 = self.mod1.line(t, r1)
		L2 = self.mod2.line(t, r2)
		return (L1 + L2)/self.L0
	"""
	
	################################################################################
	# Find quantities in average effective spacetime
	################################################################################
	
	
	def H_avg(self, t):
		"""The average Hubble rate, based on time derivatives of the average 
		   scale factor."""
		aa = self.a_avg(t)
		da = self.a_avg(t+1.) - aa # da/dt
		return da/aa
	
	def addot_avg(self, t):
		"""Double time derivative of the average scale factor."""
		# FIXME: Why is this 50? Just because the value doesn't change that much?
		aa = self.a_avg(t)
		aap = self.a_avg(t+50.)
		addot = (  aap - 2.*aa + self.a_avg(t-50.)  )/(50.**2.)
		return addot
	
	def qd_avg(self, t):
		"""Deceleration parameter in the effective averaged spacetime."""
		# q = -(\ddot{a}/a)H^-2 = - \ddot{a} * a / \dot{a}^2
		aa = self.a_avg(t)
		aap = self.a_avg(t+50.)
		adot = (aap - aa)
		addot = aap - 2.*aa + self.a_avg(t-50.)
		qq = -addot * aa / (adot)**2. # Factors of delta_t cancel
		return qq
	
	def z_avg(self, t):
		"""The average redshift in the effective model, calculated by 
		   integrating the average Hubble rate."""
		t0 = self.mod1.t(1.0)
		I, err = scipy.integrate.quad(self.H_avg, t0, t)
		return np.exp(-I) - 1.
	
	def ricci3_avg(self, t):
		"""Get average of (3)Ricci scalar."""
		rs1 = self.mod1.ricci3(t)
		rs2 = self.mod2.ricci3(t)
		V1 = self.mod1.a(t)**3. * self.comoving_V1
		V2 = self.mod2.a(t)**3. * self.comoving_V2
		return (rs1*V1 + rs2*V2) / (V1 + V2) # Weighted average
	
	def qloc_avg(self, t):
		"""Get average of the local FLRW deceleration parameter, 
		   q0 = 0.5*Omega_m - Omega_Lambda, at some time."""
		q1 = self.mod1.q(t)
		q2 = self.mod2.q(t)
		V1 = self.mod1.a(t)**3. * self.comoving_V1
		V2 = self.mod2.a(t)**3. * self.comoving_V2
		return (q1*V1 + q2*V2) / (V1 + V2) # Weighted average
	
	def rho_avg(self, t):
		"""Average matter density in the model."""
		a1 = self.mod1.a(t); z1 = (1./a1) - 1.
		a2 = self.mod2.a(t); z2 = (1./a2) - 1.
		rho1 = self.mod1.density(z1)
		rho2 = self.mod2.density(z2)
		V1 = a1**3. * self.comoving_V1
		V2 = a2**3. * self.comoving_V2
		return (V1*rho1 + V2*rho2) / (V1 + V2)
	
	
	
	################################################################################
	# Solve geodesic equation in effective averaged geometry
	################################################################################
	
	# NOTE: Don't modify these relations to use conformal Cartesian coordinates.
	# dA != a * x(a)
	
	def eff_geod_eqn_kt(self, x, t):
		"""Integrand for ODE solver for the effective geometry geodesic eqn., 
		   with k=<k>(t)."""
		aa = self.a_avg(t)
		kk = self.ricci3_avg(t) * aa**2. / (6. * C**2.) # From averaged 3-Ricci scalar
		dxdt = C * np.sqrt(1. - kk*x**2.) / aa
		return dxdt
	
	def eff_geod_eqn_k0(self, x, t):
		"""Integrand for ODE solver for the effective geometry geodesic eqn., 
		   with assumed effective spatial flatness k=0."""
		aa = self.a_avg(t)
		dxdt = C / aa
		return dxdt
	
	def eff_geod_eqn_kconst(self, x, t, k0):
		"""Integrand for ODE solver for the effective geometry geodesic eqn., 
		   with k=const=<k>(t0)."""
		aa = self.a_avg(t)
		dxdt = C * np.sqrt(1. - k0*x**2.) / aa
		return dxdt
	
	def effective_geodesic(self, t):
		"""Get comoving coordinate x(t) in the effective geometry defined by 
		   the 1D Buchert-like average line element 
		   ds^2 = dt^2 - a^2_D dx^2/(1 - <k>x^2)"""
		
		# Initial conditions
		t0 = t[0] #self.mod1.t(1.0)
		k0 = self.ricci3_avg(t0) / (6. * C**2.)
		x0 = [0.0]
		
		# Get x(t) for different choices of spatial curvature k
		x1 = scipy.integrate.odeint(self.eff_geod_eqn_kt, x0, t)
		x2 = scipy.integrate.odeint(self.eff_geod_eqn_k0, x0, t)
		x3 = scipy.integrate.odeint(self.eff_geod_eqn_kconst, x0, t, args=(k0,))
		
		return x1, x2, x3
	
	def effective_da(self, t):
		"""Get comoving coordinate x(t) in the effective geometry defined by 
		   the 1D Buchert-like average line element 
		   ds^2 = dt^2 - a^2_D dx^2/(1 - <k>x^2)"""
		
		# Initial conditions
		t0 = t[0] #self.mod1.t(1.0)
		k0 = self.ricci3_avg(t0) / (6. * C**2.)
		x0 = [0.0]
		
		# Get x(t) for different choices of spatial curvature k
		x1 = scipy.integrate.odeint(self.eff_geod_eqn_kt, x0, t)
		x2 = scipy.integrate.odeint(self.eff_geod_eqn_k0, x0, t)
		x3 = scipy.integrate.odeint(self.eff_geod_eqn_kconst, x0, t, args=(k0,))
		
		# Get average s-t scale factor for each t, and redshift from averaged scale factor
		aa = [self.a_avg(tt) for tt in t]
		aa = np.array(aa)
		zz = (1./aa) - 1.
		
		# Construct angular diameter distance
		da1 = -x1.flatten() * aa
		da2 = -x2.flatten() * aa
		da3 = -x3.flatten() * aa
		
		return da1, da2, da3, zz
	
	############################################################################
	# TESTING: Use Sachs equations instead of coordinate r(t) * a to find dA|avg
	############################################################################
	
	
	def eqn_geodesic_avg(self, y, u):
		"""RHS of linear ODEs for Sachs equations and other geodesic quantities in 
		   a given FLRW model. Quantities are:
			(0) S = sqrt(A)
			(1) X = dS/du
			(2) z(u) [redshift along LOS, not necessarily a = 1/(1+z)]
			(3) x(u)
			(4) t(u)
		"""
		# Label coordinates
		S = y[0]; X = y[1]; z = y[2]; x = y[3]; t = y[4]
		fn = [None]*len(y) # Empty array for values being returned
	
		# Calculate background quantities
		a = self.a_avg(t)
		H = self.H_avg(t)
		rho = self.rho_avg(t)
		
		# Calculate RHS of ODEs
		# FIXME: Eqn. for r(lambda) needs to be fixed.
		fn[0] = X
		fn[1] = (-4.*np.pi*G/(C**2.)) * rho * (1.+z)**2. * S
		fn[2] = -(H/C) * (1. + z)**2.
		fn[3] = 0.0 # -((1.+z) / a) * ( 1. + 0.25 * mod.Ok * (H0*x/C)**2. )
		fn[4] = (1. + z)/C
		return np.array(fn)
	
	def effective_da_sachs(self):
		"""Integrate the Sachs equations for the averaged spacetime"""
		t0 = self.t0
		y0 = np.array([0.0, 1.0, 0.0, 0.0, t0])
		#u = np.linspace(0.0, self.UMAX0*5.8, self.UCELLS)
		u = np.linspace(0.0, self.UMAX0*self.UMAX_MULT, self.UCELLS)
		y = scipy.integrate.odeint(self.eqn_geodesic_avg, y0, u)
		y = y.T # Get transpose of results matrix
		return y
		
