#!/usr/bin/python
"""Gaussian LTB model with constant bang time"""

import numpy as np
from units import *

class BLmodel1(object):
	
	def __init__(self):
		"""Bolejko and Andersson model 1"""
		
		self.h = 0.7
		self.H0 = fH0*self.h
		self.t0 = 11.4421e3
		
		# See model12.py and appendix of Bolejko and Larsson for model definition
		# Set m(r) parameters
		self.mA = -7.09548978e+10
		self.mw = 1.23018544e+00
		self.mq = 8.23964783e-01
		self.mc = 1.05969896e+11
		
		# Set k(r) parameters
		self.kA = -5.12638900e-08
		self.kw = 1.17053187e+00
		self.kq = 8.06796878e-01
		self.kc = 1.03527521e-08
	
	# Define LTB functions
	
	def m(self, r):
		"""Density-like function m"""
		return self.mA*np.exp(-(r/self.mw)**(2.*self.mq)) + self.mc
	
	def mprime(self, r):
		"""Radial derivative of density-like function"""
		return (self.m(r+0.01) - self.m(r))/0.01

	def k(self, r):
		"""Curvature-like function, k(r)"""
		return self.kA*np.exp(-(r/self.kw)**(2.*self.kq)) + self.kc

	def kprime(self, r):
		"""Radial derivative of curvature-like function, k'(r)"""
		return (self.k(r+0.01) - self.k(r))/0.01
	
	def tb(self, r):
		"""Bang-time function, Myr"""
		return 0.0
	
	def tbprime(self, r):
		"""Radial derivative of Bang-time function"""
		return 0.0
