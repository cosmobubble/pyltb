#!/usr/bin/python
"""Gaussian LTB model with constant bang time"""

import numpy as np
from units import *

class LTBGaussian(object):
	
	def __init__(self, Otot, Ok, Wk, h):
		"""Initialise void"""
		
		# Set t0 manually (shouldn't do this!)
		self.t0 = 11297.9
		
		# Set Gaussian model parameters
		self.Ak = -1.0 * Ok * (h*fH0/C)**2.0
		self.Ck = -1.0 * (Otot - Ok) * (h*fH0/C)**2.0
		self.KR = 1./(Wk*Wk)
		self.M0 = (3. / (8.*np.pi*G)) * ( (fH0*h)**2.0 + (self.Ak + self.Ck)*C**2.0 )
		self.h = h
	
	# Define LTB functions
	
	def m(self, r):
		"""Density-like function m"""
		return self.M0
	
	def mprime(self, r):
		"""Radial derivative of density-like function"""
		return 0.0

	def k(self, r):
		"""Curvature-like function, k(r)"""
		return self.Ak * np.exp(-self.KR * r*r) + self.Ck

	def kprime(self, r):
		"""Radial derivative of curvature-like function, k'(r)"""
		return self.Ak * (-2.*self.KR*r) * np.exp(-self.KR * r*r);
	
	def tb(self, r):
		"""Bang-time function, Myr"""
		return 0.0
	
	def tbprime(self, r):
		"""Radial derivative of Bang-time function"""
		return 0.0
