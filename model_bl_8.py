#!/usr/bin/python
"""Gaussian LTB model with constant bang time"""

import numpy as np
import scipy.integrate as integ
from units import *

class BLmodel8(object):
	
	def __init__(self):
		"""Bolejko and Larsson model 8"""
		
		# FIXME: Set error reporting in NumPy
		#np.seterr(all='raise')
		
		self.h = 0.7
		self.H0 = fH0*self.h
		self.Om = 0.27
		#self.t0 = 11.4421e3
		#self.t0 = 5569.307694 + 0.15
		self.t0 = 6207.88254964
		
		# k(r) variables
		self.l = 1e3 # 1 kpc^-1 in our units (=1e3 Mpc^-1)
		self.A_E = (self.H0/C)**2.
		self.beta = 1e-3 * self.l
		
		# M(r) variables
		self.A_rho = 6.2*self.Om*3.*self.H0**2. / (8.*np.pi*G)
		self.sigma = 4e-8 * self.l**2.
		self.A_M = 1.5*6.2*self.Om*self.H0**2./C**2.
		self.M0 = 0.0 # Fixed by BC that H0 finite at r=0
		
		# Define vectorised M(r)
		self.vec_M = np.vectorize(self.M)
		
		"""
		# m(r) variables
		self.m_A = 2.19440252e+11
		self.m_w = 6.56693542e+00
		self.m_c = 9.92674148e+09
		self.m_q = 9.16969555e-01
		self.m_r0 = -9.99994963e-03
		"""
		
		# tB(r) variables
		"""
		self.tb_A = -1.68426408e+03
		self.tb_w = 3.03944524e+00
		self.tb_c = 1.14375368e+04
		self.tb_q = 6.60718586e-01
		self.tb_r0 = -8.42022707e-04
		
		self.tb_A = -5.49346590e+03
		self.tb_w = 3.03945207e+00
		self.tb_c = 5.55431733e+03
		self.tb_q = 6.60719843e-01
		self.tb_r0 = -8.52444068e-04
		"""
		
		########################################################################
		"""
		# FIXME: Uncomment this.
		# Correction polynomial to improve the tB(r) fit
		coeff_tb_poly = [3.52769509e-11, -4.71251341e-09, 2.86789287e-07, 
						-1.05281948e-05, 2.60244698e-04, -4.57971422e-03, 
						5.91738319e-02, -5.71032276e-01, 4.15018865e+00, 
						-2.27737465e+01, 9.41649668e+01, -2.91196673e+02, 
						6.60202038e+02, -1.04201300e+03, 1.01350948e+03, 
						-4.65947974e+02, 5.25271798e+01]
		self.f_tb_resid = np.poly1d(coeff_tb_poly)
		"""
		########################################################################

		
		"""
		# Construct polynomials from fns fitted using fit_polynomial2.py and 
		# model8.py.
		# NOTE: Obsolete. Causes problems. We can do better by fitting exponentials.
		c_m = [ 4.87420171e+04, -1.24505293e+06, -1.86077564e+07, 8.75241477e+08, 
				-8.41548826e+09, 3.74374198e+09, 2.26529767e+11 ]
		c_k = [ -3.31489159e-06, 1.34791053e-04, -2.08217995e-03, 1.50864231e-02, 
				-5.10587792e-02, 6.87019190e-02, -2.18749041e-02 ]
		c_tb = [ 2.11839067e-03, -1.10280035e-01, 2.13785176e+00, -1.76198497e+01, 
				 3.05603567e+01, 3.79689462e+02, 9.71471023e+03 ]
		
		self.f_m = np.poly1d(c_m)
		self.f_k = np.poly1d(c_k)
		self.f_tb = np.poly1d(c_tb)
		"""
	
		
	# Define LTB functions
	
	def M_integrand(self, r):
		"""Integrand for calculating M(r)."""
		return r**2. * np.exp(-self.sigma * r**2.)

	def M(self, r):
		"""Integrate density equation at t=t0 to get M(r)."""
		# This function must be vectorised. See __init__().
		I, err = integ.quad(self.M_integrand, 0.0, r)
		return self.M0 + self.A_M*I
	
	def m(self, r):
		"""LTB mass density function m, using M = (4 pi G / 3 c^2)mr^3."""
		return (3.*C**2./(4.*np.pi*G)) * self.vec_M(r) / r**3.
	
	"""
	def m(self, r):
		"" "Density-like function m"" "
		return self.m_A*np.exp(-((r-self.m_r0)/self.m_w)**(2.*self.m_q)) + self.m_c
	"""
	
	def mprime(self, r):
		"""Radial derivative of density-like function"""
		return (self.m(r+0.01) - self.m(r))/0.01
	
	def k(self, r):
		"""Curvature-like function, k(r)"""
		return -2. * self.A_E * np.exp(self.beta * r)

	def kprime(self, r):
		"""Radial derivative of curvature-like function, k'(r)"""
		#return (self.k(r+0.01) - self.k(r))/0.01
		return -2. * self.beta * self.A_E * np.exp(self.beta * r)

################################################################################
	"""
	def tb(self, r):
		"" "Bang-time function, Myr"" "
		return self.tb_A*np.exp(-((r-self.tb_r0)/self.tb_w)**(2.*self.tb_q)) + self.tb_c
	"""

	def E(self, r):
		"""Spatial curvature function, defined in the paper."""
		EE = self.A_E * r**2. * np.exp(self.beta * r)
		if EE == 0.0:
			return self.E(1e-20)
		else:
			return EE

	def tb(self, r):
		"""Calculate [t - tB(r)] using the parametric (hyperbolic) LTB solution."""
		
		EE = self.E(r)
		MM = self.vec_M(r)
		#MM = self.m(r)*r**3. * (4.*np.pi*G)/(3.*C**2.)
		
		eta = np.arccosh(1. + (2.*EE*r/MM))
		return self.t0 - MM*(2.*EE)**(-3./2.)*(np.sinh(eta) - eta)/C
	
	def tbprime(self, r):
		"""Radial derivative of Bang-time function"""
		return (self.tb(r+0.01) - self.tb(r))/0.01
		
################################################################################
"""	
	def tb(self, r):
		"" "Bang-time function, Myr"" "
		# Fitted exponential, with a 16-degree correction polynomial to more 
		# accurately reproduce the correct shape
		tb_exp = self.tb_A*np.exp(-((r-self.tb_r0)/self.tb_w)**(2.*self.tb_q)) + self.tb_c
		return tb_exp + self.f_tb_resid(r) 
"""

	
