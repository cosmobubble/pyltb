#!/usr/bin/python
"""Define an LTB model which is actually a dust+curvature FLRW model."""
import numpy as np
from units import *

class LTBFLRW(object):
	
	def __init__(self, h, Om):
		"""Initialise the model. h is dimensionless Hubble rate today, Om is 
		matter density."""
		self.h = h
		self.Om = Om
		self.Ok = 1. - Om

	def m(self, r):
		"""Density-like function m(r) in m=const gauge"""
		return (3. / (8.*np.pi*G)) * (self.h*fH0)**2. * self.Om

	def k(self, r):
		"""Spatial curvature function k(r)"""
		return -(self.h*fH0)**2. * self.Ok / C**2.

	def tb(self, r):
		"""Bang time function (constant)"""
		return 0.0

	def mprime(self, r):
		"""Radial derivative of density-like function m(r) in m=const gauge"""
		return 0.0

	def kprime(self, r):
		"""Radial derivative of spatial curvature function k(r)"""
		return 0.0

	def tbprime(self, r):
		"""Radial derivative of bang time function (estimate using finite difference)"""
		return 0.0
		
