#!/usr/bin/python
"""Collection of FLRW models with analytic expressions for various quantities."""

import flrw
import scipy.optimize
import scipy.integrate
import numpy as np
from units import *

################################################################################
# Collapsing, matter-dominated model
################################################################################

class CollapsingMatter(flrw.FLRW):
	"""A collapsing matter+curvature FLRW model, with Omega_m > 1."""
	
	def __init__(self, h, Om, phase=0.0):
		"""A collapsing matter+curvature FLRW model, with Omega_m > 1."""
		
		# Check that Om > 1
		if Om <= 1.:
			print "ERROR: Collapsing Matter model must have Om > 1. Quitting."
			exit()
		
		# Initialise base FLRW class
		super(CollapsingMatter, self).__init__(h=h, Om=Om, Ol=0.0)
		
		# Maximum scale factor and time
		self.amax = self.Om/(self.Om - 1.) # Maximum scale factor (when H=0)
		self.tmax = self.t_a(self.amax) # The time at which the expansion is at maximum
		self.tcrunch = 2.*self.tmax
		
		# Start time from phase
		self.tstart = phase * self.tcrunch
	
	
	def t_a(self, a):
		"""Cosmic time as a function of scale factor, a, found by solving the 
		   Friedmann equation. See Mathematica notebook 
		   FLRW_collapsing_model_analytic_solution.nb."""
		
		# Solve for t(a)
		tt = np.sqrt(  (self.Om * a * (self.Om + a - self.Om*a))/(-1. + self.Om)  )
		tt -= np.sqrt(  (self.Om**3. * a * (self.Om + a - self.Om*a))/(-1. + self.Om)  )
		tt += self.Om**1.5 * np.arcsin(  np.sqrt(  ((-1. + self.Om)*a)/self.Om   )  )
		tt /= abs(self.H0) * np.sqrt(  (-1. + self.Om)**3. * self.Om   )
		return tt
	
	def t(self, a, expanding=True):
		"""Cosmic time as a function of scale factor a, with sensitivity to 
		   sign of the Hubble rate (expanding or collapsing phase)."""
		
		tt = self.t_a(a) # Get time for this scale factor assuming expanding phase
		
		# Return the time with the correct sign
		if expanding:
			# Expanding phase (0 < t < tmax)
			return tt
		else:
			# Collapsing phase (tcrunch > t > tmax)
			return self.tcrunch - tt
			
	
	def a(self, t):
		"""Find a(t) by inverting t(a) numerically. Could be slow."""
		
		# Change zero point of time evolution
		t = t + self.tstart
		
		if t < self.tmax:
			aa = scipy.optimize.brentq(lambda x: self.t(x, expanding=True) - t, 
										0.0, self.amax)
		elif t >= self.tmax and t < self.tcrunch:
			aa = scipy.optimize.brentq(lambda x: self.t(x, expanding=False) - t, 
										0.0, self.amax)
		else:
			aa = 0.0
		return aa
	
	def dadt(self, t):
		"""Time derivative of a(t), calculated numerically."""
		return self.a(t+1.) - self.a(t)
	
	"""
	def volume(self, t, rD):
		"" "Get the volume of a spatial domain at time t."" "
		a = self.a(t)
		kk = -self.Ok*(self.H0/C)**2.
		vol = a**3. * np.arcsin( rD * np.sqrt(kk) ) / np.sqrt(kk) # Closed
		return vol
	"""
	
	# In progress...
	def volume(self, t, rD):
		"""Get the volume of a spatial domain at time t (using conformal 
		   Cartesian coords), where the domain has coordinate size rD."""
		a = self.a(t)
		return a**3. * self.comoving_volume(rD)
	
	def coord_x(self, chi):
		"""Get coordinate distance x as a function of comoving distance chi."""
		kk = 0.5 * np.sqrt( -self.Ok * (self.H0/C)**2. )
		x = np.arctanh(kk * chi) / kk
		return x
	
	def comov_chi(self, x):
		"""Get comoving distance chi as a function of coordinate distance x."""
		# See Mathematica notebook FLRW_conformal_Cartesian.nb
		kk = 0.5 * np.sqrt( -self.Ok * (self.H0/C)**2. )
		chi = np.arctanh(kk * x) / kk
		return chi
	# End
	
	def line(self, t, rD):
		"""Get the (comoving) line volume of a spatial domain at time t."""
		aa = self.a(t)
		kk = -self.Ok*(self.H0/C)**2.
		vol = aa * np.arcsin( rD * np.sqrt(kk) ) / np.sqrt(kk) # Closed
		return vol
	
	def ricci3(self, t):
		"""3-Ricci scalar (function of spatial curvature)"""
		kk = -self.Ok * (self.H0/C)**2.
		aa = self.a(t)
		return 6. * kk * C**2. / aa**2.
	
	def q(self, t):
		"""Get the local FLRW deceleration parameter at this time."""
		aa = self.a(t)
		zz = (1./aa) - 1.
		hh = self.H(zz)
		q = (self.H0/hh)**2. * ( 0.5*self.Om * aa**(-3.) - self.Ol )
		return q



################################################################################
# Milne (open, empty) FLRW model
################################################################################

class Milne(flrw.FLRW):
	
	def __init__(self, h):
		"""An empty (Milne) FLRW model, with no matter."""
		
		# Initialise base FLRW class
		super(Milne, self).__init__(h=h, Om=0.0, Ol=0.0)
		
	def t(self, a):
		"""Cosmic time as a function of scale factor, a."""
		return a / self.H0
	
	def a(self, t):
		"""Scale factor as a function of cosmic time."""
		return self.H0 * t
	
	"""
	def volume(self, t, rD):
		"" "Get the volume of a spatial domain at time t."" "
		a = self.a(t)
		kk = -self.Ok*(self.H0/C)**2.
		vol = a**3. * np.arcsinh( rD * np.sqrt(-kk) ) / np.sqrt(-kk) # Open
		return vol
	"""
	
	
	# In progress...
	def volume(self, t, rD):
		"""Get the volume of a spatial domain at time t (using conformal 
		   Cartesian coords), where the domain has coordinate size rD."""
		a = self.a(t)
		return a**3. * self.comoving_volume(rD)
	
	def coord_x(self, chi):
		"""Get coordinate distance x as a function of comoving distance chi."""
		kk = 0.5 * np.sqrt( self.Ok * (self.H0/C)**2. )
		x = np.arctan(kk * chi) / kk
		return x
	
	def comov_chi(self, x):
		"""Get comoving distance chi as a function of coordinate distance x."""
		# See Mathematica notebook FLRW_conformal_Cartesian.nb
		kk = 0.5 * np.sqrt( self.Ok * (self.H0/C)**2. )
		chi = np.arctan(kk * x) / kk
		return chi
	# End
	
	
	
	def line(self, t, rD):
		"""Get the (comoving) line volume of a spatial domain at time t."""
		aa = self.a(t)
		kk = -self.Ok*(self.H0/C)**2.
		vol = aa * np.arcsinh( rD * np.sqrt(-kk) ) / np.sqrt(-kk) # Open
		return vol
	
	def ricci3(self, t):
		"""3-Ricci scalar (function of spatial curvature)"""
		kk = -self.Ok * (self.H0/C)**2.
		aa = self.a(t)
		return 6. * kk * C**2. / aa**2.
	
	def q(self, t):
		"""Get the local FLRW deceleration parameter at this time."""
		aa = self.a(t)
		zz = (1./aa) - 1.
		hh = self.H(zz)
		q = (self.H0/hh)**2. * ( 0.5*self.Om * aa**(-3.) - self.Ol )
		return q



################################################################################
# Einstein-de Sitter (flat, matter-only) FLRW model
################################################################################

class EdS(flrw.FLRW):
	
	def __init__(self, h):
		"""A flat, matter-only (EdS) FLRW model."""
		
		# Initialise base FLRW class
		super(EdS, self).__init__(h=h, Om=1.0, Ol=0.0)
		
		# Initial time (time when a(t) = 1); fixed to be +ve
		self.ti = 2./(3. * abs(self.H0))
		
	def t(self, a):
		"""Cosmic time as a function of scale factor, a."""
		return self.ti + (2./(3.*self.H0)) * (a**1.5 - 1.)
	
	def a(self, t):
		"""Scale factor as a function of cosmic time."""
		return (1. + 1.5*self.H0*(t - self.ti))**(2./3.)
	
	"""
	def volume(self, t, rD):
		"" "Get the volume of a spatial domain at time t."" "
		a = self.a(t)
		vol = rD * a**3.
		return vol
	"""
	
	
	# In progress...
	def volume(self, t, rD):
		"""Get the volume of a spatial domain at time t (using conformal 
		   Cartesian coords), where the domain has coordinate size rD."""
		a = self.a(t)
		return a**3. * self.comoving_volume(rD)
	
	def coord_x(self, chi):
		"""Get coordinate distance x as a function of comoving distance chi."""
		return chi
	
	def comov_chi(self, x):
		"""Get comoving distance chi as a function of coordinate distance x."""
		return x
	# End
	
	
	
	def line(self, t, rD):
		"""Get the (comoving) line volume of a spatial domain at time t."""
		aa = self.a(t)
		vol = aa * rD
		return vol
	
	def ricci3(self, t):
		"""3-Ricci scalar (function of spatial curvature)"""
		return 0.0
	
	def q(self, t):
		"""Get the local FLRW deceleration parameter at this time."""
		aa = self.a(t)
		zz = (1./aa) - 1.
		hh = self.H(zz)
		q = (self.H0/hh)**2. * ( 0.5*self.Om * aa**(-3.) - self.Ol )
		return q

