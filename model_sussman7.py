#!/usr/bin/python
"""Define an LTB model which should have an effective acceleration when Buchert-averaged (following the guidelines in Section 7 of Sussman's 2011 paper)."""
import numpy as np
from units import *

class LTBSussman7(object):
	
	def __init__(self, h, r0, Ak):
		"""Initialise the model"""
		self.h = h
		self.r0 = r0
		self.Ak = Ak
		
		# Get time t=ti of initial hypersurface, where a1(r, ti) = 1
		eta0 = np.arccosh( 1. - ( 3. * self.k(0.) / (4.*np.pi*G*self.m(0.)) ) )
		self.ti = (4.*np.pi*G*self.m(0.) / (3. * C**3. * (-self.k(0.))**1.5)) * (np.sinh(eta0) - eta0)
		print self.ti

	def m(self, r):
		"""Density-like function m(r) in m=const gauge"""
		return (3. / (8.*np.pi*G)) * (self.h*fH0)**2.

	def k(self, r):
		"""Spatial curvature function k(r)"""
		return self.Ak * ( np.exp( -(r/self.r0)**2. ) - 1.01  )

	def tb(self, r):
		"""Bang time function (calculated from gauge considerations and fixing a1(r,ti)=1)"""
	
		# LTB analytic solution parameter (given that a1(r, t=ti)=1)
		eta = np.arccosh( 1. - ( 3. * self.k(r) / (4.*np.pi*G*self.m(r)) ) )
	
		# Now calculate tB(r) given that a1(r, ti) = 1
		return self.ti - (4.*np.pi*G*self.m(r) / (3. * C**3. * (-self.k(r))**1.5)) * (np.sinh(eta) - eta)

	def mprime(self, r):
		"""Radial derivative of density-like function m(r) in m=const gauge"""
		return 0.0

	def kprime(self, r):
		"""Radial derivative of spatial curvature function k(r)"""
		return -2. * self.Ak * (r / (self.r0**2.)) * np.exp(-(r/self.r0)**2.)

	def tbprime(self, r):
		"""Radial derivative of bang time function (estimate using finite difference)"""
		return self.tb(r+1.) - self.tb(r)
		
