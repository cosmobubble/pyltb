#!/usr/bin/python

import scipy.integrate
import scipy.optimize
import scipy.interpolate
import numpy as np
from units import *

class FLRW(object):
	"""A generic FLRW model."""
	
	def __init__(self, h, Om, Ol):
		"""Initialise the model"""
		
		# Set constants for model
		#self.C = 0.30659458 # Speed of light in Mpc/Myr
		#self.G = 4.4986e-21 # Gravitational constant in Mpc^3 Msun^-1 Myr^-2
		#self.H0 = h * 1.02269e-4 # H0 in Myr^-1
		self.H0 = h * fH0 # H0 in Myr^-1
		self.Om = Om # Omega_matter
		self.Ol = Ol # Omega_lambda
		self.Ok = 1. - Om - Ol # Curvature k
	
	
	############################################################################
	# Background quantities
	############################################################################
	
	def H(self, z):
		"""Hubble rate for a given z"""
		return self.H0 * np.sqrt(self.Om*(1.+z)**3. + self.Ol + self.Ok*(1.+z)**2.)
	
	def density(self, z):
		"""Matter density at some redshift z"""
		return self.Om * (1. + z)**3. * (3.*self.H0**2. / (8.*np.pi*G))
	
	
	############################################################################
	# Geodesic quantities
	############################################################################
	
	def dl_integrand(self, z):
		"""The integrand for the integration required to evaluate dL(z)"""
		return 1. / np.sqrt(self.Om*(1.+z)**3. + self.Ol + self.Ok*(1.+z)**2.)

	def dL(self, z):
		"""Find the luminosity distance in LCDM for a given redshift z"""
		return (1. + z) * (C/self.H0) * scipy.integrate.romberg(self.dl_integrand, 0.0, z, divmax=100)

	def dA(self, z):
		"""Find the angular diameter distance in LCDM for a given redshift z"""
		return self.dL(z)/(1.+z)**2.
	
	def deltaDM(self, z):
		"""Find the difference in distance modulus between a Milne universe and LCDM"""
		return 5. * np.log10( self.dL(z) / ((C/self.H0) * (z + 0.5*z**2.)) )
	
	############################################################################
	# Other observables
	############################################################################
	
	def AlcockPaczynski(self, z):
		"""Calculate the Alcock-Paczynski (1979) measure"""
		# Consistency check: Should be equal to 1 at z=0
		return self.dA(z) * self.H(z) / (z*C)
	
	def q0(self):
		"""Deceleration parameter q0."""
		# See http://en.wikipedia.org/wiki/Deceleration_parameter
		q = 0.5*self.Om - self.Ol
		return q
	
	############################################################################
	# Quantities for spatial averaging
	############################################################################
	
	def comoving_volume(self, rD):
		"""Comoving volume size, in conformal Cartesian coords, for cubic 
		   domain of side rD."""
		kk = -self.Ok * (self.H0/C)**2.
		I, err = scipy.integrate.tplquad( lambda x,y,z: 1./(1.-0.25*kk*(x**2.+y**2.+z**2.))**3., 0., rD, lambda x: 0., lambda x: rD, lambda x,y: 0., lambda x,y:rD )
		return I
	
	"""
	def volume(self, t, rD):
		"" "Get the volume of a spatial domain at time t."" "
		# FIXME: Need to implement a(t) function for all FLRW models.
		a = self.a(t)
		kk = self.Ok*(self.H0*r/C)**2.
		if kk > 0.0:
			vol = np.arcsin( rD * np.sqrt(-kk) ) / np.sqrt(-kk) # Closed
		elif kk < 0.0: # Check this!!!! sign of k, open, closed
			vol = np.arcsinh( rD * np.sqrt(-kk) ) / np.sqrt(-kk) # Open
		else:
			vol = rD # Flat
		
		vol *= a**3.
		return vol
	"""
		
