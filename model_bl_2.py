#!/usr/bin/python
"""Gaussian LTB model with constant bang time"""

import numpy as np
from units import *

class BLmodel2(object):
	
	def __init__(self):
		"""Bolejko and Larsson model 2"""
		
		self.h = 0.7
		self.H0 = fH0*self.h
		self.t0 = 11.4421e3
		
		# See model12.py and appendix of Bolejko and Larsson for model definition
		# Set m(r) parameters
		self.mA = -5.63370832e+10
		self.mw = 6.86173109e-01
		self.mq = 8.13105756e-01
		self.mc = 9.15946706e+10
		
		# Set k(r) parameters
		self.kA = -4.26164031e-08
		self.kw = 6.47131108e-01
		self.kq = 7.75218583e-01
		self.kc = 1.01308834e-09
	
		
	# Define LTB functions
	
	def m(self, r):
		"""Density-like function m"""
		return self.mA*np.exp(-(r/self.mw)**(2.*self.mq)) + self.mc
	
	def mprime(self, r):
		"""Radial derivative of density-like function"""
		return (self.m(r+0.01) - self.m(r))/0.01

	def k(self, r):
		"""Curvature-like function, k(r)"""
		return self.kA*np.exp(-(r/self.kw)**(2.*self.kq)) + self.kc

	def kprime(self, r):
		"""Radial derivative of curvature-like function, k'(r)"""
		return (self.k(r+0.01) - self.k(r))/0.01
	
	def tb(self, r):
		"""Bang-time function, Myr"""
		return 0.0
	
	def tbprime(self, r):
		"""Radial derivative of Bang-time function"""
		return 0.0
