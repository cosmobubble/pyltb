#!/usr/bin/python
"""Definition of a void model"""
import numpy as np
import scipy.optimize as opt

# Units: Mass (Msun); Time (Myr); Distance (Mpc)
G = 4.4986e-21
C = 0.30659458
GYR = 1000.
fH0 = 1.02269e-4
A1_REC = 1e-10
T_REC = 1e-10
FLAT_RATIO = 1e-4 # k/m ratio below which we consider the curvature to be flat

class LTB(object):
	"""Calculate background quantities for a given model in LTB spacetime."""
	
	def __init__(self, model, drstep=1e-4, sgn=1.):
		"""Connect the LTB object to a model class"""
		# Governs interval for finite difference differentiation. May need to 
		# change it depending on most natural distance scale of model.
		self.dr = drstep
		self.sgn = sgn
		
		self.m = model.m
		self.mprime = model.mprime
		self.k = model.k
		self.kprime = model.kprime
		self.tb = model.tb
		self.tbprime = model.tbprime
	
	############################################################################
	# Define observable functions
	############################################################################
	
	def Ht(self, r, t):
		"""Transverse Hubble rate"""
		#return 100.0 * self.a1dot(r,t) / (fH0 * self.a1(r,t))
		return self.a1dot(r,t) / self.a1(r,t)
	
	def Hr(self, r, t):
		"""Radial Hubble rate"""
		#return 100.0 * self.a2dot(r,t) / (fH0 * self.a2(r,t))
		return self.a2dot(r,t) / self.a2(r,t)
	
	def density(self, r, t):
		"""Physical density"""
		# Factor of 3?
		return (3.*self.m(r) + self.mprime(r)*r) / ( 3.*self.a2(r,t)*(self.a1(r,t)**2.) )
	
	def q_ks(self, r, t):
		"""The Kristian-Sachs observational deceleration parameter, Q, defined 
		   in Clarkson and Umeh 2011."""
		ht = self.Ht(r,t)
		hr = self.Hr(r,t)
		qq = 3. / (2.*ht + hr)**2.
		qq *= 4.*np.pi*G*self.density(r, t) + 2.*( hr - ht )**2.
		return qq
	
	def q_local(self, r, t):
		"""The local volume deceleration parameter, q_theta, defined in 
		   Clarkson and Umeh 2011."""
		rho = self.density(r,t)
		ht = self.Ht(r,t)
		hr = self.Hr(r,t)
		k = self.k(r)
		kprime = self.kprime(r)
		a1 = self.a1(r,t)
		a2 = self.a2(r,t)
		
		ricci3 = ( 3./(a1*a2) ) * ( k*(2. + (a2/a1)) + kprime*r )
		shr = (hr - ht)**2.
		qloc = 12.*np.pi*G*rho + 2.*shr
		qloc /= 24.*np.pi*G*rho + shr - ricci3*C**2.
		return qloc
		
	
	############################################################################
	# Define background functions
	############################################################################

	def ftheta_open(self, p, c):
		"""Parametric eqn which defines theta=p in the hyperbolic (k<0) case. 
		   Find its zero to find theta."""
		return c - np.sinh(p) + p

	def ftheta_closed(self, p, c):
		"""Parametric eqn which defines theta=p in the elliptic (k>0) case. 
		   Find its zero to find theta."""
		#print "ftheta_closed: p =", p, "/ fn val. =", c + np.sin(p) - p
		return c + np.sin(p) - p

	def ftheta_constant(self, mm, kk, tt):
		"""Calculate the constant which must be evaluated to find theta"""
		return 3. * C**3. * tt * abs(kk)**1.5 / ( 4.*np.pi*G*mm )

	def analytic_a1_flat(self, mm, tt):
		"""Analytic solution for a1(r,t) in the flat/parabolic case"""
		return (6.*np.pi*G*mm*tt**2.)**(1./3.)

	def analytic_a1_closed(self, mm, kk, p):
		"""Analytic solution for a1(r,t) in the closed/elliptic case"""
		return 4.*np.pi*G*mm*(1. - np.cos(p)) / (3. * C**2.0 * kk)

	def analytic_a1_open(self, mm, kk, p):
		"""Analytic solution for a1(r,t) in the open/hyperbolic case"""
		return 4.*np.pi*G*mm*(1. - np.cosh(p)) / (3. * C**2. * kk)

	def theta(self, r, t):
		"""The parameter theta used in the LTB equations"""
		mm = self.m(r)
		kk = self.k(r)
		tt = t - self.tb(r)
		c = self.ftheta_constant(mm, kk, tt)
		if kk > 0.0:
			# Closed, parabolic
			p = opt.brentq(self.ftheta_closed, -5., 5., args=c)
		else:
			# Open, hyperbolic
			p = opt.brentq(self.ftheta_open, -30., 30., args=c)
		return p
	
	def a2(self, r, t):
		"""Numerically differentiate a1 to get a2(r,t)"""
		aa = self.a1(r, t)
		return aa + r * (self.a1(r+self.dr, t) - aa)/self.dr
	
	def a2dot(self, r, t):
		"""Numerically differentiate a1dot to get a2dot(r,t)"""
		aa = self.a1dot(r, t)
		return aa + r * (self.a1dot(r+self.dr, t) - aa)/self.dr
	
	def a2r(self, r, t):
		"""Numerically differentiate a2 to get d^2/dr^2(a1 r)"""
		# Use the forward difference to estimate the 2nd derivative, to avoid 
		# having to use np.abs(), which may be slow.
		# Uses dr = 1.0
		aa = self.a1(r, t)
		aap = self.a1(r+1., t)
		aapp = self.a1(r+2., t)
		a1prime = aap - aa
		a1primeprime = aapp - 2.*aap + aa
		return r*a1primeprime + 2.*a1prime
	
	"""
	def a2_analytic(self, r, t):
		"" "Find a2 in the open case. Warning - assumes gauge choice of m'=0!"" "
		
		# FIXME: Doesn't give expected results, like a1(r=0,t) = a2(r=0,t) (when Atb != 0).
		# Also suffers from spikes in the solutions, but these are probably due 
		# to the closed case not being handled. Needs debugging.
		
		print "WARNING: a2_analytic(r,t) is broken somehow"
		
		mm = self.m(r)
		kk = self.k(r)
		tt = t - self.tb(r)
	
		# Classify curvature
		ratio = abs( 3.*kk*C**2. / (8.*np.pi*G*mm) )
		if ratio < FLAT_RATIO:
			# Flat, parabolic (k=0)
			print "ERROR: Not implemented a2 for flat case yet"
			exit(-1)
			return 1.0
	
		# Use correct function for each sign of curvature and return a1(r,t)
		c = self.ftheta_constant(mm, kk, tt)
		if kk > 0.0:
			# Closed, parabolic
			p = opt.brentq(self.ftheta_closed, -5., 5., args=c)
			A = np.cos(p)
			B = np.sin(p)
			f1 = 1. / ( 4. * (A-1.) * kk**2. * r**4. )
			f2 = (4.*A - B**2. + 3.*B*p -4.)
			f2 *= ( -8.*np.pi*G*mm*r**3. / (3.*C**2.) )
			f2 *= self.kprime(r)*r**2. + 2.*kk*r
			f3 = +32.*np.pi*G*mm*r**2. / (C**2.)
			f3 *= (A + 0.5*B*p -1.) * (kk*r**2.)
			f4 = +4. * B * self.tbprime(r) * C
			f4 *= (kk*r**2.)**1.5
		else:
			# Open, hyperbolic
			p = opt.brentq(self.ftheta_open, -5., 5., args=c)
			A = np.cosh(p)
			B = np.sinh(p)
			Aminus = np.cosh(p) - 1.
			f1 = 1. / ( 4. * Aminus * kk**2. * r**4. )
			f2 = (4.*Aminus + B**2. - 3.*B*p)
			f2 *= ( -8.*np.pi*G*mm*r**3. / (3.*C**2.) )
			f2 *= self.kprime(r)*r**2. + 2.*kk*r
			f3 = -32.*np.pi*G*mm*r**2. / (C**2.)
			f3 *= (A - 0.5*B*p -1.) * (-kk*r**2.)
			f4 = -4. * B * self.tbprime(r) * C
			f4 *= (-kk*r**2.)**1.5
		a2val = f1 * (f2 + f3 + f4)
		return a2val
	"""

	def a1(self, r, t):
		"""Find a1(r,t) for different cases of curvature, and evaluate the 
		   parameter p if necessary."""
	
		# Evaluate and cache m(r), k(r), t-tB(r) to avoid multiple evaluations
		mm = self.m(r)
		kk = self.k(r)
		tt = t - self.tb(r)
	
		# Classify curvature
		ratio = abs( 3.*kk*C**2. / (8.*np.pi*G*mm) )
		if ratio < FLAT_RATIO:
			# Flat, parabolic (k=0)
			return self.analytic_a1_flat(mm, tt)
	
		# Use correct function for each sign of curvature and return a1(r,t)
		c = self.ftheta_constant(mm, kk, tt)
		if kk > 0.0:
			# Closed, elliptic
			try:
				#print "\tc =", c, "/ ratio =", ratio, "/ k =", kk, "/ m =", mm
				p = opt.brentq(self.ftheta_closed, -10., 10., args=c)
			except:
				print "FAILED! c =", c
				p = opt.brentq(self.ftheta_closed, -30., 30., args=c)
			return self.analytic_a1_closed(mm, kk, p)
		else:
			# Open, hyperbolic
			try:
				p = opt.brentq(self.ftheta_open, -10., 10., args=c)
			except:
				#print "FAILED!"
				p = opt.brentq(self.ftheta_open, -100., 100., args=c)
			return self.analytic_a1_open(mm, kk, p)

	def a1dot(self, r, t):
		"""Find a1dot, the time derivative of the transverse scale factor"""
		aa = self.a1(r,t)
		f = (8.*np.pi*G*self.m(r)/(3.*aa)) - self.k(r)*C**2.
		f = self.sgn * np.sqrt(f)
		return f
	
	def theta_a1(self, r, t):
		"""Find theta calculated in a1(r,t) for different cases of curvature."""
	
		# Evaluate and cache m(r), k(r), t-tB(r) to avoid multiple evaluations
		mm = self.m(r)
		kk = self.k(r)
		tt = t - self.tb(r)
	
		# Classify curvature
		ratio = abs( 3.*kk*C**2. / (8.*np.pi*G*mm) )
		if ratio < FLAT_RATIO:
			# Flat, parabolic (k=0)
			return 0.0
	
		# Use correct function for each sign of curvature and return a1(r,t)
		c = self.ftheta_constant(mm, kk, tt)
		if kk > 0.0:
			# Closed, elliptic
			try:
				#print "\tc =", c, "/ ratio =", ratio, "/ k =", kk, "/ m =", mm
				p = opt.brentq(self.ftheta_closed, -10., 10., args=c)
			except:
				print "FAILED! c =", c
				p = opt.brentq(self.ftheta_closed, -30., 30., args=c)
			return p
		else:
			# Open, hyperbolic
			try:
				p = opt.brentq(self.ftheta_open, -10., 10., args=c)
			except:
				#print "FAILED!"
				p = opt.brentq(self.ftheta_open, -100., 100., args=c)
			return p
